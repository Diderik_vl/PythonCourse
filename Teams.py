from Player import Goalkeeper
from Player import Defender
from Player import Midfielder
from Player import Attacker


class Teams(object):

    def make_fcb(self):
        ter_stegen = Goalkeeper("Ter Stegen", 85, 0)
        roberto = Defender("Roberto", 83, 0)
        pique = Defender("Pique", 91, 0)
        umtiti = Defender("Umtiti", 87, 0)
        alba = Defender("Roberto", 87, 0)
        busquets = Midfielder("Busquets", 90, 89)
        iniesta = Midfielder("Iniesta", 93, 86)
        rakitic = Midfielder("Rakitic", 85, 89)
        messi = Attacker("Messi", 0, 99)
        suarez = Attacker("Suarez", 0, 92)
        coutinho = Attacker("Coutinho", 0, 89)

        team = ["FC Barcelona", ter_stegen, alba, pique, umtiti, roberto, busquets, iniesta, rakitic, messi, suarez, coutinho]
        print(team[0] + " Made")
        return team

    def make_rm(self):
        navas = Goalkeeper("Navas", 83, 0)
        carvajal = Defender("Carvajal", 85, 0)
        ramos = Defender("Ramos", 91, 0)
        varane = Defender("Varane", 84, 0)
        marcelo = Defender("Marcelo", 86, 0)
        casemiro = Midfielder("Casemiro", 87, 85)
        kroos = Midfielder("Kroos", 93, 86)
        modric = Midfielder("Modric", 85, 89)
        bale = Attacker("Bale", 0, 99)
        benzema = Attacker("Benzema", 0, 85)
        ronaldo = Attacker("Ronaldo", 0, 97)

        team = ["Real Madrid", navas, carvajal, ramos, varane, marcelo, casemiro, kroos, modric, bale, benzema, ronaldo]
        print(team[0] + " Made")
        return team

    def make_mci(self):
        ederson = Goalkeeper("Ederson", 85, 0)
        walker = Defender("Walker", 87, 0)
        otamendi = Defender("Lovren", 91, 0)
        stones = Defender("Van Dijk", 87, 0)
        kompany = Defender("Kompany", 83, 0)
        danilo = Defender("Danilo", 90, 0)
        fernandinho = Midfielder("Fernandinho", 89, 84)
        silva = Midfielder("Silva", 90, 80)
        de_bruyne = Midfielder("De Bruyne", 90, 85)
        aguero = Attacker("Aguero", 0, 86)
        jesus = Attacker("Jesus", 0, 85)

        team = ["Manchester City", ederson, walker, otamendi, stones, kompany, danilo, fernandinho, silva, de_bruyne, aguero, jesus]
        print(team[0] + " Made")
        return team

    def make_liv(self):
        karius = Goalkeeper("Karius", 85, 0)
        arnold = Defender("Alexander-Arnold", 87, 0)
        lovren = Defender("Lovren", 91, 0)
        van_dijk = Defender("Van Dijk", 87, 0)
        robertson = Defender("Robertson", 83, 0)
        henderson = Midfielder("Henderson", 90, 89)
        wijnaldum = Midfielder("Wijnaldum", 93, 86)
        milner = Midfielder("Milner", 85, 89)
        salah = Attacker("Salah", 0, 90)
        firmino = Attacker("Firmino", 0, 86)
        mane = Attacker("Mane", 0, 85)

        team = ["Liverpool", karius, arnold, lovren, van_dijk, robertson, henderson, wijnaldum, milner, salah, firmino, mane]
        print(team[0] + " Made")
        return team
