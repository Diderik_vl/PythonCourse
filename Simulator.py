import Player
import random
import LoadObjects
from threading import Thread


class GameSimulator(object):
    def __init__(self, team_1, team_2):
        self.team_1 = team_1
        self.team_2 = team_2

    def set_teams(self, team_1, team_2):
        self.team_1 = team_1
        self.team_2 = team_2

    def get_teams(self):
        return self.team_1, self.team_2

    def simulate(self):
        team_1 = TeamBuilder(self.team_1)
        team_1.start()
        team_2 = TeamBuilder(self.team_2)
        team_2.start()

        team_1.join()
        team_2.join()

        home_avg = team_1.get_avg()
        away_avg = team_2.get_avg()

        goals_home = self.calc_goals(home_avg[1], away_avg[0])
        goals_away = self.calc_goals(away_avg[1], home_avg[0])

        if goals_home > goals_away:
            winstr = str(team_1.get_team_name()) + " heeft gewonnen! De uitslag was: " + str(goals_home) + " - " + str(goals_away)
        elif goals_home < goals_away:
            winstr = str(team_2.get_team_name()) + " heeft gewonnen! De uitslag was: " + str(goals_home) + " - " + str(goals_away)
        else:
            winstr = "Het is een gelijkspel! De uitslag was: " + str(goals_home) + " - " + str(goals_away)

        return winstr

    def calc_goals(self, att, dfnd):
        dfrnc = att - dfnd
        rand = random.uniform(0, 1)
        goals = dfrnc * rand
        goals = int(round(goals))
        if goals < 0:
            goals = 0
        else:
            pass
        return goals


class TeamBuilder(Thread):
    def __init__(self, team):
        Thread.__init__(self)
        self.__team = team
        self.__name = None
        self.__avg = None

    def set_team_name(self, name):
        self.__name = name

    def set_avg(self, avg):
        self.__avg = avg

    def get_team_name(self):
        return self.__name

    def get_avg(self):
        return self.__avg

    def run(self):
        loader = LoadObjects.Loader(self.__team)
        loader.make_teams()
        team = loader.get_team()

        self.set_team_name(self.team_name(team))
        self.set_avg(self.calc_avg(team))

    def team_name(self, team):
        name = team[0]
        return name

    def calc_avg(self, team):
        dfnd_tot = 0
        att_tot = 0
        dfnders = 0
        atters = 0

        for player in team:
            if isinstance(player, Player.Player):
                if isinstance(player, (Player.Goalkeeper, Player.Defender, Player.Midfielder)):
                    dfnd_tot = dfnd_tot + player.get_dfnd()
                    dfnders += 1
                if isinstance(player, (Player.Midfielder, Player.Attacker)):
                    att_tot = att_tot + player.get_att()
                    atters += 1

        dfnd_avg = dfnd_tot / dfnders
        att_avg = att_tot / atters
        return dfnd_avg, att_avg

