from Simulator import GameSimulator
import socket

hostname = ''
port = 5000

close_msg = "Je tegenstander heeft de verbinding afgesloten."

s = socket.socket()
s.bind((hostname, port))

while True:
    s.listen(2)
    client_1, address_1 = s.accept()
    print("Connection from: " + str(address_1))
    client_2, address_2 = s.accept()
    print("Connection from: " + str(address_2))
    count = 0

    while True:
        count += 1
        team_1 = client_1.recv(1024).decode()
        team_2 = client_2.recv(1024).decode()

        if team_1 == '0' or team_2 == '0':
            client_1.sendall(close_msg.encode('utf-8'))
            client_2.sendall(close_msg.encode('utf-8'))
            client_1.close()
            client_2.close()
            s.close()
            break

        print("Wedstrijd " + str(count) + " wordt gespeeld: (" + str(team_1) + ", " + str(team_2) + ").")
        simulator = GameSimulator(team_1, team_2)
        winstr = simulator.simulate()
        print(winstr)

        client_1.sendall(winstr.encode('utf-8'))
        client_2.sendall(winstr.encode('utf-8'))

    break

s.close()
print("Connections closed")





























