import pickle
from Teams import Teams


class Loader(object):
    def __init__(self, team):
        self.__team = team

    def get_team(self):
        return self.__team

    def set_team(self, team):
        self.__team = team

    def make_teams(self):
        if self.__team is '1':
            self.__team = self.load_fcb()
        elif self.__team is '2':
            self.__team = self.load_rm()
        elif self.__team is '3':
            self.__team = self.load_mci()
        elif self.__team is '4':
            self.__team = self.load_liv()


    def load_fcb(self):
        team = Teams()
        try:
            fcb = pickle.load(open("fcb.p", "rb"))
        except:
            print("Making FC Barcelona")
            fcb = team.make_fcb()
            pickle.dump(fcb, open("fcb.p", "wb"))
        return fcb

    def load_rm(self):
        team = Teams()
        try:
            rm = pickle.load(open("rm.p", "rb"))
        except:
            print("Making Real Madrid")
            rm = team.make_rm()
            pickle.dump(rm, open("rm.p", "wb"))
        return rm

    def load_mci(self):
        team = Teams()
        try:
            mci = pickle.load(open("mci.p", "rb"))
        except:
            print("Making Manchester City")
            mci = team.make_mci()
            pickle.dump(mci, open("mci.p", "wb"))
        return mci

    def load_liv(self):
        team = Teams()
        try:
            liv = pickle.load(open("liv.p", "rb"))
        except:
            print("Making Liverpool")
            liv = team.make_liv()
            pickle.dump(liv, open("liv.p", "wb"))
        return liv
