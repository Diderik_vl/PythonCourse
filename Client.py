import socket
import File

hostname = socket.gethostname()
port = 5000

s = socket.socket()
file = File.ScoreSheet()


def receive(team):
    s.sendall(team.encode('utf-8'))
    score = s.recv(1024).decode()
    file.add_to_file(score)
    print(score)
    pass


def spelen(count):
    while True:
        if count == 0:
            print("We gaan een wedstrijd spelen!")
            count += 1
        else:
            print("Voer 0 in om te stoppen")

        while True:
            boolean = 5

            print("Kies een team:")
            print("0. Stoppen")
            print("1. FC Barcelona")
            print("2. Real Madrid")
            print("3. Manchester City")
            print("4. Liverpool")
            team = input("->")

            try:
                boolean = int(team)
            except:
                print("Alleen getallen invullen!")

            if 0 <= boolean <= 4:
                break
            elif 0 > boolean > 4:
                print("Alleen getallen van 0 tot 4")

        if boolean == 0:
            receive(team)
            break
        else:
            receive(team)
    try:
        file.write()
    except:
        print("File could not be written, closing now")
    s.close()


try:
    s.connect((hostname, port))
    spelen(0)
except:
    print("De server is momenteel niet online, probeer het later nogmaals")

