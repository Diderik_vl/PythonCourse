class Player(object):
    def __init__(self, name, dfnd, att):
        self.__name = name
        self.__dfnd = dfnd
        self.__att = att

    def get_name(self):
        return self.__name

    def set_name(self, name):
        self.__name = name


class Goalkeeper(Player):
    def __init__(self, naam, dfnd, att):
        super().__init__(naam, dfnd, att)
        self.__dfnd = dfnd

    def get_dfnd(self):
        return self.__dfnd

    def set_dfnd(self, dfnd):
        self.__dfnd = dfnd


class Defender(Player):
    def __init__(self, naam, dfnd, att):
        super().__init__(naam, dfnd, att)
        self.__dfnd = dfnd

    def get_dfnd(self):
        return self.__dfnd

    def set_dfnd(self, dfnd):
        self.__dfnd = dfnd


class Attacker(Player):
    def __init__(self, naam, dfnd, att):
        super().__init__(naam, dfnd, att)
        self.__att = att

    def get_att(self):
        return self.__att

    def set_att(self, att):
        self.__att = att


class Midfielder(Defender, Attacker):
    def __init__(self, naam, dfnd, att):
        super().__init__(naam, dfnd, att)
        self.__naam = naam
        self.__dfnd = dfnd
        self.__att = att

