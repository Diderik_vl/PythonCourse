import datetime


class ScoreSheet(object):
    def __init__(self):
        self.__tekst = ''

    def set_tekst(self, tekst):
        self.__tekst = tekst

    def get_tekst(self):
        return self.__tekst

    def add_to_file(self, tekst):
        self.__tekst += tekst + "\n"

    def write(self):
        date = datetime.datetime.now()
        filename = "Scoresheet-" + date.strftime("%y-%m-%d-%H-%M-%S") + ".txt"
        file = open(filename, 'w')
        file.write(self.__tekst)
        file.close()
